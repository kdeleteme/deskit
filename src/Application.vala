class Deskit.DApplication : Gtk.Application {

	private DMainWindow main_window;
		
	public DApplication() {
		Object(application_id: Strings.APPLICATION_ID,
			   flags: ApplicationFlags.FLAGS_NONE);
	}
		
	public override void activate() {
		main_window = new DMainWindow(this);
		main_window.show_all();
	}

	public static int main(string[] args) {
		Gtk.init(ref args);
		var app = new DApplication();

		return app.run(args);
	}
}