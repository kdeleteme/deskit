namespace Deskit.Util {
	delegate void SubExistsCallback(bool exists);
	delegate void SubGetPostsCallback(GLib.List<Data.Post> posts,
									  string? after);
}

class Deskit.Util.SubFetcher : Deskit.Util.Fetcher {
	private string? posts_after;

	public void
	get_posts(string? sub_name, string? after, owned SubGetPostsCallback cb) {
		var url = "";

		if (sub_name != null && sub_name.length > 0) {
			url += Strings.BASE_URL + "/r/" + sub_name +
			"/.json";
		} else {
			url += Strings.BASE_URL + "/.json";
		}

		if (after != null) {
			url += "?after=" + after;
		}

		var msg = new Soup.Message("GET", url);

		session.queue_message(msg, (sess, mess) => {

				var posts = parse_posts((string) mess.response_body.data);
				cb(posts, posts_after);

				msg.response_body.free();
				msg.response_headers.free();
				msg.request_body.free();
				msg.request_headers.free();				
			});
	}

	public void
	check_exists(string sub_name, owned Util.SubExistsCallback e) {
		var exists = false;
		var url = Strings.BASE_URL + @"/subreddits/search.json?q=$sub_name";

		var msg = new Soup.Message("GET", url);

		session.queue_message(msg, (sess, mess) => {

				var parser = new Json.Parser();
				try {
					parser.load_from_data((string) msg.response_body.data);
				} catch(Error e) {
					stderr.printf("Error searching sub: %s\n", e.message);
				} finally {
					var node = parser.get_root().get_object();
					
					if (node.has_member("data")) {
						var data = node.get_object_member("data");
						var children = data.get_array_member("children");

						if (children.get_length() > 0) {
							var result = children.get_object_element(0);
							var child_data = result.get_object_member("data");
							var display_name = child_data.get_string_member(
								"display_name");
							
							exists = display_name.down() == sub_name.down();
						}
					}
				}

				e(exists);

				msg.response_body.free();
				msg.response_headers.free();
				msg.request_body.free();
				msg.request_headers.free();
			});
	}

	private GLib.List<Data.Post> parse_posts(string json_string) {
		var new_posts = new GLib.List<Data.Post>();

		try {
			var parser = new Json.Parser();
			parser.load_from_data(json_string);

			var node = parser.get_root().get_object();
			var data = node.get_member("data").get_object();
			var elements = data.get_array_member("children").get_elements();
			posts_after = data.get_string_member("after");
			
			foreach (var element in elements) {
				var child_node = element.get_object();

				var child_data = child_node.get_member("data").get_object();
				var title = child_data.get_string_member("title");
				var author = child_data.get_string_member("author");
				var sub = child_data.get_string_member("subreddit");
				var thumbnail_uri = child_data.get_string_member("thumbnail");
				var permalink = child_data.get_string_member("permalink");
				var selftext = child_data.get_string_member("selftext");
				var url = child_data.get_string_member("url");
				var is_video = child_data.get_boolean_member("is_video");

				var post = new Data.Post();
				post.title = title;
				post.author = @"u/$author";
				post.sub = @"r/$sub";
				post.permalink = permalink;
				post.url = url;
				post.is_video = is_video;

				if (child_data.has_member("post_hint")) {
					var post_hint = child_data.get_string_member("post_hint");
					post.is_image = (post_hint == "image");
				} else {
					post.is_image = ".jpg" in url || ".png" in url;
				}

				if (thumbnail_uri.length > 0 && (thumbnail_uri != "self" ||
												 thumbnail_uri != "default" ||
												 thumbnail_uri != "image")) {
					post.thumbnail_uri = thumbnail_uri;
				}

				if (selftext.length > 0) {
					post.selftext = selftext;
				}

				new_posts.append(post);
			}
		} catch(Error e) {
			stderr.printf("Error: %s\n", e.message);
		}

		return new_posts;
	}
}