class Deskit.DHeaderBar : Gtk.HeaderBar {
	private Gtk.MenuButton add_sub_btn;
	private Gtk.Button back_btn;
	private Gtk.MenuButton sort_subs_btn;
	private Gtk.Button sort_subs_asc_btn;
	private Gtk.Button sort_subs_desc_btn;
	private Gtk.Popover sort_subs_popover;
	private Gtk.Popover add_sub_popover;
	private Gtk.Entry add_sub_entry;
	private Gtk.Button submit;

	private DMainWindow main_window;

	private const int ENTRY_LABEL_MARGIN = 8;

	public DHeaderBar(DMainWindow parent) {
		Object();
		this.main_window = parent;
	}

	construct {
		title = Strings.APPLICATION_NAME;
		show_close_button = true;

		build_back_btn();
		build_add_sub_btn();
		build_sort_subs_btn();

		connect_signals();
	}

	public void back_btn_activate() {
		back_btn.sensitive = true;
	}

	public void back_btn_deactivate() {
		back_btn.sensitive = false;
	}

	private void build_back_btn() {
		back_btn = new Gtk.Button();
		back_btn.sensitive = false;
		back_btn.tooltip_text = "Go Back";
		back_btn.image = new Gtk.Image.from_icon_name("go-previous-symbolic",
													  Gtk.IconSize.BUTTON);
		pack_start(back_btn);
	}

	private void build_add_sub_btn() {
		add_sub_btn = new Gtk.MenuButton();
		add_sub_btn.tooltip_text = "Add a Sub";
		add_sub_btn.image = new Gtk.Image.from_icon_name(
			"list-add-symbolic", Gtk.IconSize.BUTTON);

		add_sub_popover = new Gtk.Popover(add_sub_btn);
		add_sub_entry = new Gtk.Entry();

		var add_sub_container = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
		add_sub_container.border_width = 8;
			
		var entry_label = new Gtk.Label("<b>r/</b>");
		entry_label.use_markup = true;
		entry_label.margin_end = ENTRY_LABEL_MARGIN;
			
		submit = new Gtk.Button.from_icon_name("object-select-symbolic",
											   Gtk.IconSize.BUTTON);
		submit.relief = Gtk.ReliefStyle.NONE;
		submit.tooltip_text = "Done";

		add_sub_container.pack_start(entry_label, false, false, 0);
		add_sub_container.pack_start(add_sub_entry, false, false, 0);
		add_sub_container.pack_end(submit, false, false, 0);
		add_sub_container.show_all();
			
		add_sub_popover.add(add_sub_container);
		add_sub_btn.popover = add_sub_popover;
		pack_start(add_sub_btn);
	}

	private void build_sort_subs_btn() {
		sort_subs_btn = new Gtk.MenuButton();
		sort_subs_btn.label = "Sort";
		sort_subs_btn.tooltip_text = "Sort Subs";
		sort_subs_asc_btn = new Gtk.Button.from_icon_name(
			"view-sort-ascending-symbolic", Gtk.IconSize.BUTTON);
		sort_subs_asc_btn.label = "Ascending";
		sort_subs_asc_btn.tooltip_text = "Sort Ascending";
		sort_subs_asc_btn.always_show_image = true;
		sort_subs_asc_btn.relief = Gtk.ReliefStyle.NONE;
		sort_subs_desc_btn = new Gtk.Button.from_icon_name(
			"view-sort-descending-symbolic", Gtk.IconSize.BUTTON);
		sort_subs_desc_btn.label = "Descending";		
		sort_subs_desc_btn.tooltip_text = "Sort Descending";
		sort_subs_desc_btn.always_show_image = true;
		sort_subs_desc_btn.relief = Gtk.ReliefStyle.NONE;
		sort_subs_popover = new Gtk.Popover(sort_subs_btn);
		var sort_subs_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
		sort_subs_container.border_width = 8;
		sort_subs_container.pack_start(sort_subs_asc_btn, false, false, 0);
		sort_subs_container.pack_start(sort_subs_desc_btn, false, false, 0);
		sort_subs_container.show_all();
		sort_subs_popover.add(sort_subs_container);
		sort_subs_btn.popover = sort_subs_popover;

		pack_start(sort_subs_btn);
	}
	 

	private void connect_signals() {
		add_sub_entry.activate.connect(on_add_sub_entry_activate);
		submit.clicked.connect(on_submit_clicked);
		back_btn.clicked.connect(on_back_clicked);
		sort_subs_asc_btn.clicked.connect(on_sort_asc_clicked);
		sort_subs_desc_btn.clicked.connect(on_sort_desc_clicked);
	}

	private void on_back_clicked() {
		back_btn.sensitive = false;
		main_window.hide_post();
	}

	private void on_add_sub_entry_activate() {
		add_sub_if_exists();
	}

	private void on_submit_clicked() {
		add_sub_if_exists();
	}

	private void on_sort_asc_clicked() {
		main_window.sort_subs(false);
		sort_subs_popover.popdown();
	}

	private void on_sort_desc_clicked() {
		main_window.sort_subs(true);
		sort_subs_popover.popdown();
	}

	private void add_sub_if_exists() {
		main_window.add_sub_if_exists(add_sub_entry.text);
		add_sub_entry.text = "";
		add_sub_popover.popdown();
	}
}